<?php declare(strict_types=1);
namespace ChainShop\Products;

class DrinkProductType implements ProductType
{
    public function asString(): string
    {
        return 'drink';
    }
}
