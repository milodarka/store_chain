<?php declare(strict_types=1);
namespace ChainShop\Products;

class MedicineProductType implements ProductType
{
    public function asString(): string
    {
        return 'medicine';
    }
}
