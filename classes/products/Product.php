<?php

namespace ChainShop\Products;

use ChainShop\Products\ProductPrice;
use ChainShop\Products\ProductType;
use ChainShop\Products\ProductName;
use ChainShop\Products\ProductQuantity;
use Exception;

class Product
{
    private ProductName $name;
    private ProductType $type;
    private ProductPrice $productPrice;
    private ProductQuantity $quantity;
    private $serialNo = '';

    public function __construct(ProductName $name, ProductType $type, ProductPrice $productPrice, ProductQuantity $quantity, $serialNo)
    {
        $this->name = $name;
        $this->type = $type;
        $this->quantity = $quantity;
        $this->checkProductTypeForSerialNo($type, $serialNo);
        $this->productPrice = $productPrice;
        $this->checkProductQuantity($quantity);
    }

    public function setProductType($type)
    {
        $this->type = $type;
    }
    public function setProductQuantity($quantity)
    {
        $this->quantity = $quantity;
    }
    public function setProductSerialNo($serialNo)
    {
        $this->serialNo = $serialNo;
    }

    public function getProductName()
    {
        return $this->name;
    }
    public function getProductType()
    {
        return $this->type;
    }
    public function getProductQuantity()
    {
        return $this->quantity;
    }
    public function getProductPrice()
    {
        return $this->productPrice;
    }
    public function getProductSerialNo()
    {
        return $this->serialNo;
    }

    private function checkProductTypeForSerialNo(ProductType $type, $serialNo)
    {
        $serialNmbr = rand(10, 10000);
        if ($type instanceof MedicineProductType || $type instanceof ParkingTicketsProductType) {
            $this->setProductSerialNo($serialNmbr);
        } else {
            $this->serialNo = $serialNo;
        }
    }
    /**
     * @throws \Exception
     */
    private function checkProductQuantity(ProductQuantity $quantity)
    {
        if ($quantity > $this->getProductQuantity()) {
            throw new \Exception('No more products on the stock');
        } else {
            $this->setProductQuantity($quantity);
        }
    }
}