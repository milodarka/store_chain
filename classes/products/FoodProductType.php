<?php declare(strict_types=1);
namespace ChainShop\Products;

class FoodProductType implements ProductType
{
    public function asString(): string
    {
        return 'food';
    }
}
