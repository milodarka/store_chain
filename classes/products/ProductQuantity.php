<?php declare(strict_types=1);

namespace ChainShop\Products;

class ProductQuantity
{
    private int $quantityValue;

    public static function fromInt(int $quantity): ProductQuantity
    {
        return new self($quantity);
    }

    public function asInt(): int
    {
        return $this->quantityValue;
    }

    public function equals(ProductQuantity $quantity): bool
    {
        return $this->asInt() === $quantity->asInt();
    }

    public function __construct(int $quantity)
    {
        $this->quantityValue = $quantity;
    }
}
