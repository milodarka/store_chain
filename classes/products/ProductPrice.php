<?php

declare(strict_types=1);

namespace ChainShop\Products;

use ChainShop\Products\Product;

class ProductPrice extends Product
{

    private int $price;

    public static function fromInt(int $price): ProductPrice
    {
        return new self($price);
    }

    public function __construct(int $price)
    {
        $this->ensureIsPositiveInteger($price);
        $this->price = $price;
    }

    public function asInt(): int
    {
        return $this->price;
    }

    public function isGreaterThan(ProductPrice $limit): bool
    {
        return $this->price > $limit->asInt();
    }

    /**
     * @throws \InvalidArgumentException
     */
    private function ensureIsPositiveInteger(int $price): void
    {
        if ($price < 0) {
            throw new \InvalidArgumentException('Positive Integer value expected');
        }
    }
}