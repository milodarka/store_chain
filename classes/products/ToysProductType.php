<?php declare(strict_types=1);
namespace ChainShop\Products;

class ToysProductType implements ProductType
{
    public function asString(): string
    {
        return 'toys';
    }
}
