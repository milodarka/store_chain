<?php declare(strict_types=1);
namespace ChainShop\Products;

class ParkingTicketsProductType implements ProductType
{
    public function asString(): string
    {
        return 'parking tickets';
    }
}
