<?php declare(strict_types=1);
namespace ChainShop\Products;

interface ProductType
{
    public function asString(): string;
}