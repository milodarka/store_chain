<?php

declare(strict_types=1);

namespace ChainShop\Customer;

class Customer
{
    private $firstName;
    private $lastName;
    private $telephoneNo;


    public function __construct(string $firstName, string $lastName, string $telephoneNo)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->telephoneNo = $telephoneNo;
    }
    public function getFirstName(): string
    {
        return $this->firstName;
    }
    public function getLastName(): string
    {
        return $this->lastName;
    }
    public function getTelephoneNo(): string
    {
        return $this->telephoneNo;
    }
}