<?php

declare(strict_types=1);

namespace ChainShop\Shop;

use DateTime;
use DatePeriod;
use DateInterval;

class PharmasyShopType implements ShopType
{
    private $name = "Pharmasy";

    public function __construct($name)
    {
        $this->name = $name;
    }
    public function asString(): string
    {
        return $this->name;
    }
    public static function fromString(string $name): PharmasyShopType
    {
        return new self($name);
    }
    public static function report(DateTime $start, DateTime $end, $days)
    {
        $end = $end->modify('+' . $days . 'day');

        $interval = new DateInterval('P' . $days . 'D');
        $daterange = new DatePeriod($start, $interval, $end);

        foreach ($daterange as $date) {
            echo $date->format("dmY") . "<br>";
        }
    }
}