<?php

namespace ChainShop\Shop;

use DateTime;

interface ShopType

{
    public function asString(): string;
    public static function fromString(string $name): ShopType;
    public static function report(DateTime $start, DateTime $end , $days);
}