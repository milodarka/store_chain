<?php

namespace ChainShop\Shop;

use Exception;
use ChainShop\Bill\Bill;
use ChainShop\Shop\ShopName;
use ChainShop\Shop\ShopType;
use ChainShop\Products\Product;
use ChainShop\Customer\Customer;
use ChainShop\Products\ProductName;
use ChainShop\Products\ProductType;
use ChainShop\Products\ProductPrice;
use ChainShop\Products\ProductQuantity;
use ChainShop\Products\MedicineProductType;
use ChainShop\Products\CigaretteProductType;

class Shop
{
    private ShopName $name;
    private ShopType $type;
    private Product $product;


    public function __construct(ShopName $name, ShopType $type, Product $product)
    {
        $this->name = $name;
        $this->checkShopTypeSellingProduct($type, $product);
    }
    public function getShopName()
    {
        return $this->name;
    }
    public function getShopType()
    {
        return $this->type;
    }
    public function setShopType($type)
    {
        $this->type = $type;
    }

    private Customer $customer;

    public function sell(ProductPrice $price, ProductType $type, ProductName $name, ProductQuantity $qantity, Customer $customer)
    {
        $date = date("d.m.Y");
        $serialNo = '';
        $product = new Product($name, $type, $price, $qantity, $serialNo);
        $bill = new Bill(self::createShop($this->name, $this->type, $product), $date, $customer, $price, $qantity);
    }
    public static function createShop(ShopName $name, ShopType $type, Product $product): Shop
    {
        return new self($name, $type, $product);
    }


    private function checkShopTypeSellingProduct(ShopType $type, Product $product): void
    {
        $productType = $product->getProductType();
        if ($productType instanceof CigaretteProductType) {
            $type = $type::fromString('Corner Shop');
            $this->setShopType($type);
        } elseif ($productType instanceof MedicineProductType) {
            $type = $type::fromString('Pharmasy');
            $this->setShopType($type);
        } else {
            $this->type = $type;
        }
    }
}