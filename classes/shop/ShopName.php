<?php declare(strict_types=1);

namespace ChainShop\Shop;

use InvalidArgumentException;

class ShopName
{
    private $name;
    public static function fromString(string $name): ShopName
    {
        return new self($name);
    }

    public function __construct(string $name)
    {
        $this->ensureIsValidString($name);
        $this->name = $name;
    }

    public function asString(): string
    {
        return $this->name;
    }

    private function ensureIsValidString(string $name): void
    {
        if (trim($name) === '') {
            throw new InvalidArgumentException('Shop name must be a valid string');
        }
    }
}