<?php

declare(strict_types=1);

namespace ChainShop\Shop;

use DateTime;
use DatePeriod;
use DateInterval;

class CornerShopType implements ShopType
{
    private $name = "Corner Shop";

    public function __construct($name)
    {
        $this->name = $name;
    }
    public function asString(): string
    {
        return $this->name;
    }
    public static function fromString(string $name): CornerShopType
    {
        return new self($name);
    }
    public static function report(DateTime $start, DateTime $end, $days)
    {
        $end = $end->modify('+' . $days . 'day');

        $interval = new DateInterval('P' . $days . 'D');
        $daterange = new DatePeriod($start, $interval, $end);

        foreach ($daterange as $date) {
            echo $date->format("dmY") . "<br>";
        }
    }
}