<?php

namespace ChainShop\Bill;

use ChainShop\Shop\Shop;
use ChainShop\Shop\CornerShopType;
use ChainShop\Shop\ShopType;
use ChainShop\Shop\SupermarketShopType;
use ChainShop\Shop\PharmasyShopType;
use ChainShop\Customer\Customer;
use ChainShop\Products\ProductPrice;
use ChainShop\Products\Product;
use ChainShop\Products\ProductQuantity;

class Bill
{
    private Shop $shop;
    private $date;
    private Customer $customer;
    private ProductPrice $productPrice;
    private ProductQuantity $productQuantity;

    public function __construct(Shop $shop, $date, Customer $customer, ProductPrice $productPrice, ProductQuantity $productQuantity)
    {
        $this->shop = $shop;
        $this->date = $date;
        $this->customer = $customer;
        $this->productPrice = $productPrice;
        $this->productQuantity = $productQuantity;
    }

    private Product $product;
    public function createBill(ShopType $shopType, Product $product): Bill
    {
        $number = self::generateBillNumber($shopType);
        $date = $this->date;
        $customer = $this->customer;
        $product = new Product($product->getProductName(), $product->getProductType(), $product->getProductPrice(), $product->getProductQuantity(), $product->getProductSerialNo());
        return new self($this->shop, $date, $customer, $product->getProductPrice(), $product->getProductQuantity());
    }
    private static function generateBillNumber($shopType)
    {
        $no = 1;
        if (self::checkIfBeginningOfyear(date('Y-m-d'))) {
            $number = self::getSequence(1);
        } else {
            $number = self::getSequence($no);
            $no++;
        }

        switch ($shopType) {
            case $shopType instanceof CornerShopType:
                $number = 'CS_' . $number;
                break;
            case $shopType instanceof SupermarketShopType:
                $number = 'SM_' . $number;
                break;
            case $shopType instanceof PharmasyShopType:
                $number = 'PH_' . $number;
                break;
        }
        return $number;
    }

    public static function getSequence($num)
    {
        return sprintf("%'.010d\n", $num);
    }
    public static function checkIfBeginningOfyear($date)
    {
        $m = (int)date('n', strtotime($date));
        if ($m == 1) {
            return true;
        } else {
            return false;
        }
    }
}