<?php

use ChainShop\Bill\Bill;
use ChainShop\Shop\Shop;
use ChainShop\Shop\ShopName;
use ChainShop\Products\Product;
use ChainShop\Customer\Customer;
use ChainShop\Shop\CornerShopType;
use ChainShop\Products\ProductName;
use ChainShop\Products\ProductPrice;
use ChainShop\Shop\PharmasyShopType;
use ChainShop\Products\FoodProductType;
use ChainShop\Products\ProductQuantity;
use ChainShop\Shop\SupermarketShopType;
use ChainShop\Products\MedicineProductType;
use ChainShop\Products\CigaretteProductType;

require_once realpath("vendor/autoload.php");
//customer
$customer = new Customer('Milena', 'Markovic', '0654540284');

//pharmasy shop type
$shopName1 = new ShopName('shop1');
$shopType1 = new PharmasyShopType('Pharmasy');
$productName1 = new ProductName('medicine');
$productType1 = new MedicineProductType();
$serialNo1 = '';
$productPrice1 = new ProductPrice(30);
$productQuantity1 = new ProductQuantity(22);
$date1 = date("d-m-Y");
$product1 = new Product($productName1, $productType1, $productPrice1, $productQuantity1, $serialNo1);
$shop1 = new Shop($shopName1, $shopType1, $product1);
$selling1 = $shop1->sell($productPrice1, $productType1, $productName1, $productQuantity1, $customer);
$bill1 = new Bill($shop1, $date1, $customer, $productPrice1, $productQuantity1);



//cornerShop shop type
$shopName2 = new ShopName('shop2');
$shopType2 = new CornerShopType("Corner Shop");
$productName2 = new ProductName('cigarette');
$productType2 = new CigaretteProductType();
$serialNo2 = '';
$productPrice2 = new ProductPrice(258);
$productQuantity2 = new ProductQuantity(10);
$date2 = date("d-m-Y");
$product2 = new Product($productName2, $productType2, $productPrice2, $productQuantity2, $serialNo2);
$shop2 = new Shop($shopName2, $shopType2, $product2);
$selling2 = $shop2->sell($productPrice2, $productType2, $productName2, $productQuantity2, $customer);
$bill2 = new Bill($shop2, $date2, $customer, $productPrice2, $productQuantity2);


//supermarket shop type
$shopName3 = new ShopName('shop3');
$shopType3 = new SupermarketShopType("Supermarket");
$productName3 = new ProductName('food');
$productType3 = new FoodProductType();
$serialNo3 = '';
$productPrice3 = new ProductPrice(25);
$productQuantity3 = new ProductQuantity(2);
$date3 = date("d-m-Y");
$product3 = new Product($productName3, $productType3, $productPrice3, $productQuantity3, $serialNo3);
$shop3 = new Shop($shopName3, $shopType3, $product3);
$selling3 = $shop3->sell($productPrice3, $productType3, $productName3, $productQuantity3, $customer);
$bill3 = new Bill($shop3, $date3, $customer, $productPrice3, $productQuantity3);

print_r($bill1);
print_r($bill2);
print_r($bill3);
die;